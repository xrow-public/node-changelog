#!/bin/bash
source <(curl -s https://gitlab.com/xrow-public/ci-tools/-/raw/3.0/scripts/library.sh)
ci_dev_env
ci_mkdocs_lint
ci_mkdocs_build