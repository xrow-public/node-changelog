#!/usr/bin/env ts-node

import axios from 'axios';
import * as dotenv from "dotenv";

dotenv.config();

// Allow to access the environment variables
if (!('GITLAB_TOKEN' in process.env)) {
    throw new Error("Set GITLAB_TOKEN to receive changelog")
}

const TOKEN = process.env.GITLAB_TOKEN
const CI_PROJECT_ID = process.env.CI_PROJECT_ID;

const config = {
    "headers": {
        ['Content-Type']: 'application/json',
        ['PRIVATE-TOKEN']: TOKEN,
        ['Accept-Encoding']: "gzip,deflate,compress"
    }
}

const fs = require('fs');
const path = require('path');
const { exec } = require('child_process');

// Get current branch name
exec('git rev-parse --abbrev-ref HEAD', async (err, branchName) => {
    if (err) onError(err);

    const branch = branchName.trim();
    const dir = path.resolve(process.cwd()) + '/docs'
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, { recursive: true });
    }
    if (!fs.existsSync(dir + '/changelog.md')) {
        fs.appendFileSync(dir + '/changelog.md', '# Changelog\n\n' );
    }
    try {
        axios.get(
            'https://gitlab.com/api/v4/projects/' + CI_PROJECT_ID + '/releases',
            config,
        ).then(response => {
            response.data.forEach((item) => {
                var text = '## Release ' + item.name + ' (' + item.commit.short_id + ')\n\n' + item.description + '\n\n'
                console.log(text);
                fs.appendFileSync(dir + '/changelog.md', text);
            });
        })

    } catch (err) {
        console.log(err)
        throw new Error(err)
    }
});

const onError = (err) => {
    console.error(err);
    return 0;
}
